
Feature: Statement
  In order to interpret a statements
  As a developer
  I want to identify users and actions

  Scenario: Identify User
    Given a statement set to "As a user1, I want to be able to combine 2 statement into a file."
    When I want to check if a user exists
    Then the result should equal "True"

  Scenario Outline: more directories
    Given a statement set to <statement>
    When I want to check if a user exists
    Then the result should equal <result>

    Examples:
      | statement                                                        |  result |
      | As a user, I want to be able to combine 2 statement into a file. |    True |
      | This is just a random statement.                                 |   False |
