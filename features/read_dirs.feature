
Feature: Read Directory
  In order to navigate a directory
  As a developer
  I want to read directories

  Scenario: First directory
    Given a directory set to "./bin"
    When I read the directory
    Then the result should contain "cucumber.sh"

  Scenario Outline: more directories
    Given a directory set to <dir>
    When I read the directory
    Then the result should contain <result>

    Examples:
      | dir   | increment | result      |
      | ./bin |         5 | cucumber.sh |
