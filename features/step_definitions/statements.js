
const { Given, When, Then } = require("@cucumber/cucumber");
const assert = require("assert").strict

Given('a statement set to {string}', function (string) {
  this.remember(this.consider.a.statement(string));
});

When('I want to check if a user exists', function () {
  this.remember(this.memory.pop().hasUser());
});

Then('the result should equal {string}', function (string) {
  assert.equal(this.memory.pop(), string.toLowerCase() === 'true');
});


