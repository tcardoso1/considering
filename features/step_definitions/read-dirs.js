// features/support/steps.js
const { Given, When, Then } = require("@cucumber/cucumber");
const assert = require("assert").strict

Given('a directory set to {string}', function (string) {
  // Write code here that turns the phrase above into concrete actions
  //return 'pending';
  this.remember(this.consider.a.directory(string));
});

When('I read the directory', function () {
  // Write code here that turns the phrase above into concrete actions
  //return 'pending';
  this.remember(this.memory.pop().readWait());
});

Then('the result should contain {string}', function (string) {
  // Write code here that turns the phrase above into concrete actions
  //return 'pending';
  assert.equal(this.memory.pop().findWait(string).name, string);
});

