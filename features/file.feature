
Feature: Read File
  In order to read a file
  As a developer
  I want to read files

  Scenario: First File
    Given a file in "./test/test_file1.txt"
    When I read the file
    Then the contents should equal "This is just a test content."

  Scenario Outline: More files
    Given a file in <path>
    When I read the file
    Then the contents should equal <content>

    Examples:
      | path                    | content                                             |
      | "./test/test_file1.txt" | "This is just a test content."                      |
      | "./test/test_file2.txt" | "This is the first line.\nThis is the second line." |

