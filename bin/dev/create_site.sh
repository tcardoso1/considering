#!/bin/sh

# Requires gatsby to be installed (npm global package)

SITE_NAME=$1

gatsby new ${SITE_NAME} https://github.com/gatsbyjs/gatsby-starter-hello-world
