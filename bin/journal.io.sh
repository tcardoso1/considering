#!/bin/sh

# Requires journal.io to be installed (npm package)

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
API_PORT=8085 $SCRIPTPATH/../node_modules/journal.io/bin/startup.sh
