#!/bin/sh

# Requires gatsby to be installed (npm global package)
# Requires surge to be installed (npm global package)
# Requires to be already logged-in in surge using your account

gatsby build
surge public/
