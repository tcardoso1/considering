"use strict" 
/*****************************************************
 * Internal tests
 *****************************************************/

let chai = require('chai');
let chaiAsPromised = require("chai-as-promised");
let should = chai.should();
let consider = require("../main.js");
let tag = consider.tag;
let file = consider.file;
let statementsFile = consider.statementsFile;
let userStoryFile = consider.userStoryFile;
let statement = consider.statement;
let article = consider.article;
let errors = consider.errors;
let userStory = consider.userStory;
let iter = consider.iterator;
let modalVerb = consider.modalVerb;
let fs = require('fs');

before(function(done) {
  done();
});

after(function(done) {
  // here you can clear fixtures, etc.
  done();
});

describe("Considering a directory,", function() {
  it("If path does not exist should throw an error", function () {
    //Prepare
    try{
      let directory1 = consider.a.directory("/some/erroneus/path");
    } catch(e){
      e.message.should.equal('Directory "/some/erroneus/path" not found');
      return;
    }
    should.fail();
  });
  it("should be of directory instance", function () {
    //Prepare
    let file1 = consider.a.file("./test/test_file1.txt");
    (file1 instanceof file).should.equal(true);
  });
  it("should be able to search a file in the directory", function (done) {
    //Prepare
    let dir1 = consider.a.directory("./test/");
    dir1.readWait();
    dir1.find('test_file1.txt', (contents)=>{
      contents.should.be.eql({ name: 'test_file1.txt'});
      done();
    });
  });
  it("should be able to search a file (Synchronously) in the directory", function (done) {
    //Prepare
    let dir1 = consider.a.directory("./test/");
    dir1.readWait();
    //Promisify?
    let file1 = dir1.findWait('test_file1.txt');
    file1.name.should.equal('test_file1.txt');
    //contents.should.equal("This is just a test content.");
    done();
  });
  it("inexisting file search (Synchronously) should return undefined", function (done) {
    //Prepare
    let dir1 = consider.a.directory("./test/");
    dir1.readWait();
    //Promisify?
    let file1 = dir1.findWait('doesnotexist.txt');
    (file1 === undefined).should.equal(true);
    //contents.should.equal("This is just a test content.");
    done();
  });
  xit("should be possible to address it's properties via a 'where' conjunction, and have pronouns such as 'each' and 'first'.", function () {
    //Prepare
    let conjunction1 = consider.a.file("./test/test_file2.txt").where;
    conjunction1.should.not.equal(undefined);
    conjunction1.each.should.not.equal(undefined);
    conjunction1.first.should.not.equal(undefined);
  });
  xit("should be possible to get the array of lines ", function (done) {
    //Prepare
    let file1 = consider.a.file("./test/test_file2.txt")
    file1.where.each.line((content)=>{
      content.length.should.equal(2);
      done();
    });
  });
  xit("should be possible to get the first line ", function (done) {
    //Prepare
    let file1 = consider.a.file("./test/test_file2.txt")
    file1.where.first.line((data)=>{
      data.contents.should.equal("This is the first line.");
      done();
    });
  });
  xit("should be possible to get the last line ", function (done) {
    //Prepare
    let file1 = consider.a.file("./test/test_file2.txt")
    file1.where.last.line((data)=>{
      data.contents.should.equal("This is the second line.");
      done();
    });
  });
  xit("should split the file into lines", function (done) {
    //Prepare
    let statements = consider.a.file("./test/test_file2.txt").where.each.line((content)=>{
      content[0].contents.should.equal("This is the first line.");
      content[1].contents.should.equal("This is the second line.");
      done();
    });
  });
});
