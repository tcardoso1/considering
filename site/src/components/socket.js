import React from "react"
import containerStyles from "./socket.module.css"
import * as socket from "../js/communications/socket.js"

//React class component
export default class SocketViewer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { message: 'Socket waiting to connect...' };
    //Socket setup
    this.hostname = window.location.hostname;
    socket.resetSocket(this.hostname);
    let _component = this;
    socket.setCallbackOnMessage((data, status) => {
      console.log("components.socket: ", data);
      _component.showMessage(data);
    });
    socket.start(this.hostname);

    this.showMessage = this.showMessage.bind(this);
  }

  showMessage = (msg) => {
    //this.setState({message: msg});
    this.setState({message: msg});
    console.log("components.socket.showMessage: ", this.state, msg);
  }

  render = () => {
    return <div>{ this.state.message }</div>
  }
}
