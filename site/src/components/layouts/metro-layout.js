import React from "react"
import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"

import { rhythm } from "../../utils/typography2"

export default function Layout({ children }) {
    const data = useStaticQuery(
      graphql`
        query {
          site {
            siteMetadata {
              title
            }
          }
        }
      `
    )
    return (
        <div>
            <button className="button square pos-absolute pos-top-right alert" id="sidebar-toggle-2">
                <span className="mif-menu"></span>
            </button>
            <aside className="sidebar pos-absolute z-2" data-role="sidebar" data-toggle="#sidebar-toggle-2" id="sb2" data-shift=".shifted-content">
                <div className="sidebar-header" data-image="images/sb-bg-1.jpg">
                    <div className="avatar">
                        <img data-role="gravatar" data-email="tcardoso@gmail.com" />
                    </div>
                    <span className="title fg-white">Metro 4 Components Library</span>
                    <span className="subtitle fg-white"> 2018 © Sergey Pimenov</span>
                </div>
                <ul className="sidebar-menu">
                    <li><a><span className="mif-home icon"></span>Home</a></li>
                    <li><a><span className="mif-books icon"></span>Guide</a></li>
                    <li><a><span className="mif-files-empty icon"></span>Examples</a></li>
                    <li className="divider"></li>
                    <li><a><span className="mif-images icon"></span>Icons</a></li>
                </ul>
            </aside>
 
            <div className="shifted-content h-100 p-ab">
                <script src="https://cdn.metroui.org.ua/v4/js/metro.min.js"></script>
                <div className="app-bar pos-absolute bg-red z-1" data-role="appbar">
                    <button className="app-bar-item c-pointer" id="sidebar-toggle-2">
                        <span className="mif-menu fg-white"></span>
                    </button>
                </div>
                <div className="h-100 p-4">
                    {children}
                </div>
            </div>
        </div>
    )
}